import cv2
import numpy as np

image = cv2.imread("./images/rectblack.png")

cv2.imshow("original",image)
print(image)
image_ycrcb = cv2.cvtColor(image, cv2.COLOR_RGB2YCrCb)
cv2.imshow("ycbcr", image_ycrcb)

#split
y,cr,cb = cv2.split(image_ycrcb)
cv2.imshow("y", y)
cv2.imshow("cr", cr)
cv2.imshow("cb", cb)

#Y = img_ycbcr(:,:,1);


cv2.waitKey(0)